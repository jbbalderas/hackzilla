$(document).on('turbolinks:load', function(){
    $('.ui.rating').rating({
        maxRating: 5
    });

    $('.ui.dropdown').dropdown();

    $('.ui.progress').progress();

});
function openModal(modal) {
    $(modal).modal('show');
    console.log(modal);
}