class ProjectController < ApplicationController
  def potential
    # Show all the projects from freelancer
    @urlstring_to_post = 'https://www.freelancer-sandbox.com/api/projects/0.1/projects?owners[]=25832351&job_details=true'
    @result = HTTParty.get(@urlstring_to_post.to_str, 
    :headers => { 'Freelancer-OAuth-V1' => 'XUmiqZ1VHzppzkc1ykdkhGrQH5VUas' } )
    @projects = []
    @result['result']['projects'].each do |project|
      project['jobs'].each do |job|
        if job['name'] == 'Carpentry'
          @projects << project
        end
      end
    end
    @projects
  end

  def accept_project
    @project = Project.new(project_params)
    @listing = Listing.new(listing_params)
    @project.listing = @listing
    @project.state = 'unfilled'
    @project.save
    @listing.save
    # Start filling using text blast
    i = 0
    @workers = Worker.where(skill: @project.skill)
    @workers.each do |w|
      SmsHelper.send(w.subscription.access_token, w.subscription.subscriber_number, 'Job ID: ' + @listing.id.to_s)
      i = i + 1
      if i == @listing.worker_count
        break
      end
    end
    redirect_to '/projects/potential'
  end

  def unfilled
    @projects = Project.where(state: 'unfilled')
  end

  def under_bidding
    @projects = Project.where(state: 'under_bidding')
  end

  def granted
    @projects = Project.where(state: 'granted')
  end

  def listing
    @project = Project.find(params[:id])
    @listing = @project.listing
  end

  def unfilled
    @projects = Project.where(state: 'unfilled')
  end

  def under_bidding
    @projects = Project.where(state: 'under_bidding')
  end

  def granted
    @projects = Project.where(state: 'granted')
  end

  def listing
    @project = Project.find(params[:id])
    @listing = @project.listing
  end

  private
    def project_params
      params.permit(:fln_id, :name, :cost, :description)
    end
    def listing_params
      params.permit(:worker_count)
    end
end
