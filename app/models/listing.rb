class Listing < ApplicationRecord
  belongs_to :project
  has_many :workers
end
