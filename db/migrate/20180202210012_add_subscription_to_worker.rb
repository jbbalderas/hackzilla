class AddSubscriptionToWorker < ActiveRecord::Migration[5.1]
  def change
    add_reference :workers, :subscription, foreign_key: true
  end
end
