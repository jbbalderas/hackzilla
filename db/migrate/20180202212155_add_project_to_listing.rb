class AddProjectToListing < ActiveRecord::Migration[5.1]
  def change
    add_reference :listings, :project, foreign_key: true
  end
end
