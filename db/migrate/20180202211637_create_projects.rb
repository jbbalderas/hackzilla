class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.string :name
      t.integer :fln_id
      t.string :state
      t.integer :cost
      t.text :description

      t.timestamps
    end
  end
end
