class AddListingToProject < ActiveRecord::Migration[5.1]
  def change
    add_reference :projects, :listing, foreign_key: true
  end
end
